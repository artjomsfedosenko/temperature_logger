package lv.artjomsfedosenko.temperaturelogger.services;

import lv.artjomsfedosenko.temperaturelogger.schedulers.custom.Scheduler;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

//@RunWith(MockitoJUnitRunner.class)
public class ScheduleRunnerTest {

    Scheduler testScheduler;

    @Before
    public void setUp() throws Exception {

        Scheduler scheduler = mock(Scheduler.class);
        when(scheduler.getPeriod()).thenReturn(1000L);

        this.testScheduler = scheduler;
    }

    @Test
    public void testRegisteredSchedulerIsCalled() throws Exception {

        ScheduleRunner runner = new ScheduleRunner();
        runner.addScheduler(this.testScheduler);

        runner.start();

        Thread.sleep(5000);

        verify(this.testScheduler, atLeast(3)).performScheduledAction();
    }

    @Test
    public void testMultipleRegisteredSchedulersAreCalled() throws Exception {

        ScheduleRunner runner = new ScheduleRunner();
        runner.addScheduler(this.testScheduler);
        runner.addScheduler(this.testScheduler);

        runner.start();

        Thread.sleep(5000);

        verify(this.testScheduler, atLeast(4)).performScheduledAction();
    }
}