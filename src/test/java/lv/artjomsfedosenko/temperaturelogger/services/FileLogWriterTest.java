package lv.artjomsfedosenko.temperaturelogger.services;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static org.junit.Assert.*;

public class FileLogWriterTest {

    private FileLogWriter fileLogWriter;

    @Before
    public void setUp() throws Exception {

        this.fileLogWriter = new FileLogWriter("src/test/resources/test_log.txt");

    }

    @Test
    public void testWriteToLogCreatesLogFile() {

        this.fileLogWriter.writeToLog("Test message");
        File logFile = new File("src/test/resources/test_log.txt");

        assertTrue(logFile.exists());
    }

    @Test
    public void writeToLogAddsMessageToLog() throws Exception {

        this.fileLogWriter.writeToLog("Message 2");

        String expectedValue = "Message 2";
        String file ="src/test/resources/test_log.txt";

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String currentLine = reader.readLine();
        reader.close();

        assertEquals(expectedValue, currentLine);
    }

    @After
    public void tearDown() throws Exception {

        File file = new File("src/test/resources/test_log.txt");
        file.delete();
    }
}