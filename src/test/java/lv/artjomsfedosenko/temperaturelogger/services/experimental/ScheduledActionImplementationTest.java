package lv.artjomsfedosenko.temperaturelogger.services.experimental;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

public class ScheduledActionImplementationTest {

    private TestingClass testObject;

    @Before
    public void setUp() throws Exception {

        TestingClass testObject = new TestingClass();
        TestingClass spyObject = spy(testObject);

        this.testObject = spyObject;
    }

    @Test
    public void testPerformActionInvokesProvidedMethod() throws Exception {

        ScheduledAction scheduledAction = new ScheduledActionImplementation<TestingClass>(
                TestingClass.class, this.testObject, "publicMethod"
        );

        scheduledAction.performAction();

        verify(this.testObject).publicMethod();
    }

    @Test(expected = Exception.class)
    public void testInvokingPrivateMethodThrowsException() throws Exception {

        ScheduledAction scheduledAction = new ScheduledActionImplementation<TestingClass>(
                TestingClass.class, this.testObject, "privateMethod"
        );

        scheduledAction.performAction();
    }

    @Test(expected = Exception.class)
    public void testInvokingMethodOnNonExistingObjectThrowsException() throws Exception {
        ScheduledAction scheduledAction = new ScheduledActionImplementation<TestingClass>(
                TestingClass.class, null, "publicMethod"
        );

        scheduledAction.performAction();
    }
}

class TestingClass {
    public void publicMethod() {};
    private void privateMethod() {};
};