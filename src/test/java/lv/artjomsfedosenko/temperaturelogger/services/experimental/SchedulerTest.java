package lv.artjomsfedosenko.temperaturelogger.services.experimental;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.spy;

public class SchedulerTest {

    ScheduledAction scheduledAction;
    ScheduledAction anotherScheduledAction;

    @Before
    public void setUp() throws Exception {

        ScheduledAction scheduledAction = mock(ScheduledAction.class);
        this.scheduledAction = scheduledAction;

        ScheduledAction anotherScheduledAction = mock(ScheduledAction.class);
        this.anotherScheduledAction = anotherScheduledAction;
    }

    @Test
    public void testRegisteredSchedulerIsCalled() throws Exception {

        ScheduledAction scheduledAction = mock(ScheduledAction.class);

        Scheduler scheduler = new Scheduler();
        //scheduler.addScheduledAction(1000L, this.scheduledAction);
        scheduler.addScheduledAction(1000L, scheduledAction);

        scheduler.start();

        Thread.sleep(6000L);

        //verify(this.scheduledAction, atLeast(4)).performAction();
        verify(scheduledAction, atLeast(4)).performAction();

        scheduler.stop();
    }

    @Test
    public void testMultipleRegisteredSchedulersWithSamePeriod() throws Exception {

        ScheduledAction scheduledAction = mock(ScheduledAction.class);
        ScheduledAction anotherScheduledAction = mock(ScheduledAction.class);

        Scheduler scheduler = new Scheduler();
        //scheduler.addScheduledAction(1000L, this.scheduledAction);
        //scheduler.addScheduledAction(1000L, this.anotherScheduledAction);
        scheduler.addScheduledAction(1000L, scheduledAction);
        scheduler.addScheduledAction(1000L, anotherScheduledAction);

        scheduler.start();

        Thread.sleep(6000L);

        //verify(this.scheduledAction, atLeast(4)).performAction();
        //verify(this.anotherScheduledAction, atLeast(4)).performAction();
        verify(scheduledAction, atLeast(4)).performAction();
        verify(anotherScheduledAction, atLeast(4)).performAction();

        scheduler.stop();
    }

    @Test
    public void testMultipleRegisteredSchedulersWithDifferentPeriods() throws Exception {

        ScheduledAction scheduledAction = mock(ScheduledAction.class);
        ScheduledAction anotherScheduledAction = mock(ScheduledAction.class);

        Scheduler scheduler = new Scheduler();
        //scheduler.addScheduledAction(1000L, this.scheduledAction);
        //scheduler.addScheduledAction(500L, this.anotherScheduledAction);
        scheduler.addScheduledAction(1000L, scheduledAction);
        scheduler.addScheduledAction(500L, anotherScheduledAction);

        scheduler.start();

        Thread.sleep(6000L);

        //verify(this.scheduledAction, atLeast(4)).performAction();
        //verify(this.anotherScheduledAction, atLeast(8)).performAction();
        verify(scheduledAction, atLeast(4)).performAction();
        verify(anotherScheduledAction, atLeast(8)).performAction();

        scheduler.stop();
    }
}