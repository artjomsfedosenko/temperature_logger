package lv.artjomsfedosenko.temperaturelogger.schedulers.timer;

import lv.artjomsfedosenko.temperaturelogger.services.MailSender;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

public class SendTemperatureEmailTest {

    private SendTemperatureEmail sendTemperatureEmail;
    private TemperatureReader temperatureReader;
    private MailSender mailSender;

    @Before
    public void setUp() {

        TemperatureReader temperatureReader = mock(TemperatureReader.class);
        this.temperatureReader = temperatureReader;

        MailSender mailSender = mock(MailSender.class);
        this.mailSender = mailSender;

        SendTemperatureEmail sendTemperatureEmail = new SendTemperatureEmail(
                this.mailSender, this.temperatureReader
        );
        this.sendTemperatureEmail = sendTemperatureEmail;
    }

    @Test
    public void testRunCallsTemperatureReaderAndMailSender() {

        this.sendTemperatureEmail.run();

        verify(this.temperatureReader).readTemperature();
        verify(this.mailSender).sendMail(
                anyString(),
                anyString(),
                anyString()
        );
    }
}