package lv.artjomsfedosenko.temperaturelogger.schedulers.timer;

import lv.artjomsfedosenko.temperaturelogger.services.FileLogWriter;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

public class LogTemperatureTest {

    private LogTemperature logTemperature;
    private FileLogWriter fileLogWriter;
    private TemperatureReader temperatureReader;

    @Before
    public void setUp() {

        FileLogWriter fileLogWriter = mock(FileLogWriter.class);
        this.fileLogWriter = fileLogWriter;

        TemperatureReader temperatureReader = mock(TemperatureReader.class);
        this.temperatureReader = temperatureReader;

        LogTemperature logTemperature = new LogTemperature(
                this.temperatureReader, this.fileLogWriter
        );
        this.logTemperature = logTemperature;
    }

    @Test
    public void testRunCallsReadsTemperatureAndCallsLogWriter() {

        this.logTemperature.run();

        verify(this.temperatureReader).readTemperature();
        verify(this.fileLogWriter).writeToLog(anyString());
    }
}