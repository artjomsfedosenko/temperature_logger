package lv.artjomsfedosenko.temperaturelogger.schedulers;

import lv.artjomsfedosenko.temperaturelogger.schedulers.custom.LogTemperatureScheduler;
import lv.artjomsfedosenko.temperaturelogger.services.FileLogWriter;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class LogTemperatureSchedulerTest {

    private LogTemperatureScheduler scheduler;
    private FileLogWriter fileLogWriter;
    private TemperatureReader temperatureReader;

    @Before
    public void setUp() {

        FileLogWriter fileLogWriter = mock(FileLogWriter.class);
        TemperatureReader temperatureReader = mock(TemperatureReader.class);
        this.fileLogWriter = fileLogWriter;
        this.temperatureReader = temperatureReader;

        LogTemperatureScheduler logTemperatureScheduler = new LogTemperatureScheduler(
                this.temperatureReader, this.fileLogWriter
        );

        this.scheduler = logTemperatureScheduler;

    }

    @Test
    public void testGetPeriodReturnsExpectedValue() {

        Long period = this.scheduler.getPeriod();
        assertEquals(Long.valueOf(300000L), period);
    }

    @Test
    public void testPerformScheduledActionCallsDependantObjects() {

        this.scheduler.performScheduledAction();

        verify(this.temperatureReader).readTemperature();
        verify(this.fileLogWriter).writeToLog(anyString());
    }
}