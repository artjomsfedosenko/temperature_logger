package lv.artjomsfedosenko.temperaturelogger.schedulers;

import lv.artjomsfedosenko.temperaturelogger.schedulers.custom.SendTemperatureEmailScheduler;
import lv.artjomsfedosenko.temperaturelogger.services.MailSender;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;


public class SendTemperatureEmailSchedulerTest {

    private TemperatureReader temperatureReader;
    private MailSender mailSender;
    private SendTemperatureEmailScheduler scheduler;

    @Before
    public void setUp() throws Exception {

        TemperatureReader temperatureReader = mock(TemperatureReader.class);
        MailSender mailSender = mock(MailSender.class);
        this.temperatureReader = temperatureReader;
        this.mailSender = mailSender;

        SendTemperatureEmailScheduler scheduler = new SendTemperatureEmailScheduler(
                this.mailSender, this.temperatureReader
        );

        this.scheduler = scheduler;
    }

    @Test
    public void getPeriod() {

        Long period = this.scheduler.getPeriod();

        assertEquals(Long.valueOf(3600000L), period);
    }

    @Test
    public void performScheduledAction() {

        this.scheduler.performScheduledAction();

        verify(this.temperatureReader).readTemperature();
        verify(this.mailSender).sendMail(
                anyString(),
                anyString(),
                anyString()
        );
    }
}