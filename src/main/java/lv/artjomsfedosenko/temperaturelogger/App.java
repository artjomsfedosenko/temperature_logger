package lv.artjomsfedosenko.temperaturelogger;

import lv.artjomsfedosenko.temperaturelogger.schedulers.timer.LogTemperature;
import lv.artjomsfedosenko.temperaturelogger.schedulers.timer.SendTemperatureEmail;
import lv.artjomsfedosenko.temperaturelogger.services.*;

import java.util.Timer;

public class App {

    public static void main(String[] args) {

        FileLogWriter logWriter = new FileLogWriter("temperature_log.txt");
        MailSender mailSender = new MailSender();
        TemperatureReader temperatureReader = new TemperatureReader();

        LogTemperature logTemperature = new LogTemperature(
                temperatureReader, logWriter
        );

        SendTemperatureEmail sendTemperatureEmail = new SendTemperatureEmail(
                mailSender, temperatureReader
        );

        Timer timer = new Timer("Test Timer");
        timer.schedule(logTemperature, 1000L, 300000L);
        timer.schedule(sendTemperatureEmail, 5000L, 3600000L);

        ConsoleInput consoleInput = new ConsoleInput(timer, temperatureReader);
        Thread thread = new Thread(consoleInput);
        thread.start();
    }
}
