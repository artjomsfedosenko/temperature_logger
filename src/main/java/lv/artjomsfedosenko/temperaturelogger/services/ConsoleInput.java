package lv.artjomsfedosenko.temperaturelogger.services;

import java.util.Scanner;
import java.util.Timer;

public class ConsoleInput implements Runnable {

    private ScheduleRunner scheduleRunner;
    private Scanner scanner = new Scanner(System.in);
    private TemperatureReader temperatureReader;
    private Timer timer;

    public ConsoleInput(ScheduleRunner scheduleRunner, TemperatureReader temperatureReader) {
        this.scheduleRunner = scheduleRunner;
        this.temperatureReader = temperatureReader;
    }

    public ConsoleInput(Timer timer, TemperatureReader temperatureReader) {
        this.timer = timer;
        this.temperatureReader = temperatureReader;
    }

    @Override
    public void run() {
        this.promptUserInput();
    }

    private void promptUserInput() {

        System.out.println("Input \"stop\" to stop, \"temp\" to see current temperature");

        String input = this.scanner.next();

        if (input.equals("stop")) {
            this.stop();
        } else if (input.equals("temp")) {
            this.printTemperature();
        } else {
            this.promptUserInput();
        }
    }

    private void printTemperature() {

        Double temperature = this.temperatureReader.readTemperature();
        System.out.println("Temperature: " + temperature);
        this.promptUserInput();
    }

    private void stop() {
        System.out.println("Shutting down");

        if (this.scheduleRunner != null) {
            this.scheduleRunner.stop();
        }

        if (this.timer != null) {
            this.timer.cancel();
        }

        System.exit(0);
    }
}
