package lv.artjomsfedosenko.temperaturelogger.services;

import com.pi4j.component.temperature.TemperatureSensor;
import com.pi4j.io.w1.W1Master;

public class TemperatureReader {

    private W1Master w1Master = new W1Master();

    public double readTemperature() {

        ///sys/bus/w1/devices/28-80000027f79e
        Double temp = null;

        for (TemperatureSensor device : w1Master.getDevices(TemperatureSensor.class)) {

            if (device.getName().equals("28-80000027f79e")) {
                temp = device.getTemperature();
            }

            //System.out.printf("%-20s %3.1f°C %3.1f°F\n", device.getName(), device.getTemperature(),
              //      device.getTemperature(TemperatureScale.FARENHEIT));
        }


        return temp;
    }
}
