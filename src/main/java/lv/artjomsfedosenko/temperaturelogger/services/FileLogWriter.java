package lv.artjomsfedosenko.temperaturelogger.services;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class FileLogWriter {

    private String logFileName;

    public FileLogWriter(String logFileName) {
        this.logFileName = logFileName;
    }

    public void writeToLog(String message) {

        File file = new File(this.logFileName);

        try (
            FileWriter fileWriter = new FileWriter(file, true);
            PrintWriter printWriter = new PrintWriter(fileWriter);
        ) {
            printWriter.println(message + "\n");
        } catch (Exception e) {
            System.out.println("Exception while writing to file");
        }
    }
}
