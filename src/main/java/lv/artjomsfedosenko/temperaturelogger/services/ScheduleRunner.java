package lv.artjomsfedosenko.temperaturelogger.services;

import lv.artjomsfedosenko.temperaturelogger.schedulers.custom.Scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class ScheduleRunner implements Runnable {

    private Map<Scheduler, LocalDateTime> schedulers = new HashMap();
    private Thread worker;
    private AtomicBoolean running = new AtomicBoolean(false);

    public void addScheduler(Scheduler scheduler) {
        this.schedulers.put(scheduler, LocalDateTime.now());
    }

    public void start() {
        this.running.set(true);
        worker = new Thread(this);
        worker.start();
    }

    public void stop() {
        running.set(false);
    }

    @Override
    public void run() {

        while (this.running.get()) {

            for (Scheduler scheduler : this.schedulers.keySet()) {

                LocalDateTime previousTime = this.schedulers.get(scheduler);
                Long period = Duration.between(previousTime, LocalDateTime.now()).toMillis();

                if (period > scheduler.getPeriod()) {
                    scheduler.performScheduledAction();
                    this.schedulers.replace(scheduler, LocalDateTime.now());
                }
            }
        }
    }
}
