package lv.artjomsfedosenko.temperaturelogger.services;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailSender {

    private final String username = "";
    private  final String password = "";
    private final Properties properties;

    public MailSender() {

        Properties properties = new Properties();
        properties .put("mail.smtp.auth", "true");
        properties .put("mail.smtp.starttls.enable", "true");
        properties .put("mail.smtp.host", "smtp.gmail.com");
        properties .put("mail.smtp.port", "587");

        this.properties = properties;
    }

    public void sendMail(String recepient, String subject, String body) {

        Session session = Session.getInstance(this.properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("send.temperature.statistics@gmail.com"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(recepient)
            );
            message.setSubject(subject);
            message.setText(body);

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
