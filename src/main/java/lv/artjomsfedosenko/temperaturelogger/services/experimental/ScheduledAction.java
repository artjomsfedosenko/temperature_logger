package lv.artjomsfedosenko.temperaturelogger.services.experimental;

public interface ScheduledAction {

    void performAction() throws Exception;

}
