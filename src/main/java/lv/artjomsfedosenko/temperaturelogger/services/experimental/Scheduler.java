package lv.artjomsfedosenko.temperaturelogger.services.experimental;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class Scheduler implements Runnable {

    private Thread worker;
    private AtomicBoolean running = new AtomicBoolean(false);
    //Check for initialization possibilities
    private Map<Long, List<ScheduledAction>> scheduledActions = new HashMap<>();
    private Map<Long, LocalDateTime> executionTimes = new HashMap<>();

    public void addScheduledAction(Long period, ScheduledAction action) {

        if (this.scheduledActions.get(period) == null) {
            this.scheduledActions.put(period, new ArrayList<>());
        }

        List<ScheduledAction> actionList = this.scheduledActions.get(period);
        actionList.add(action);
    }

    public void start() {
        this.initExecutionTimes();
        this.running.set(true);
        worker = new Thread(this);
        worker.start();
    }

    public void stop() {
        running.set(false);
    }

    @Override
    public void run() {

        while (this.running.get()) {

            for (Long period : this.executionTimes.keySet()) {

                Long timeDiff = Duration
                        .between(this.executionTimes.get(period), LocalDateTime.now())
                        .toMillis();

                if (timeDiff >= period) {

                    this.scheduledActions.get(period)
                            .forEach(action -> this.performScheduledAction(action));
                }
            }
        }
    }

    private void initExecutionTimes() {

        LocalDateTime startTime = LocalDateTime.now();

        this.scheduledActions.keySet().forEach(
                period -> this.executionTimes.put(period, startTime)
        );
    }

    private void performScheduledAction(ScheduledAction action) {

        try {
            action.performAction();
        } catch (Exception e) {
            System.out.println("Exception while performing scheduled action");
        }
    }
}
