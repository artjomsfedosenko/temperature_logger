package lv.artjomsfedosenko.temperaturelogger.services.experimental;

import java.lang.reflect.Method;

public class ScheduledActionImplementation<T> implements ScheduledAction {

    private String methodName;
    private T object;
    private Class<T> tClass;

    public ScheduledActionImplementation(Class<T> tClass, T object, String methodName) {
        this.methodName = methodName;
        this.object = object;
        this.tClass = tClass;
    }

    @Override
    public void performAction() throws Exception {

        Method method = tClass.getMethod(this.methodName);

        if (method != null) {
            method.invoke(this.object);
        }
    }
}
