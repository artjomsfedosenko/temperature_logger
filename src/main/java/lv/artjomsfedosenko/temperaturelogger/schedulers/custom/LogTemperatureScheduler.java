package lv.artjomsfedosenko.temperaturelogger.schedulers.custom;

import lv.artjomsfedosenko.temperaturelogger.services.FileLogWriter;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;

import java.time.LocalDateTime;

public class LogTemperatureScheduler implements Scheduler {

    private TemperatureReader temperatureReader;
    private FileLogWriter fileLogWriter;

    public LogTemperatureScheduler(TemperatureReader temperatureReader, FileLogWriter fileLogWriter) {
        this.temperatureReader = temperatureReader;
        this.fileLogWriter = fileLogWriter;
    }

    @Override
    public Long getPeriod() {
        //return 300000L;
        return 6000L;
    }

    @Override
    public void performScheduledAction() {

        //read temperature
        Double temperature = this.temperatureReader.readTemperature();

        //compose message
        String logMessage = LocalDateTime.now() + " temp: " + temperature;

        //write to log
        this.fileLogWriter.writeToLog(logMessage);
    }
}
