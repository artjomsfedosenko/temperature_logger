package lv.artjomsfedosenko.temperaturelogger.schedulers.custom;

import lv.artjomsfedosenko.temperaturelogger.services.MailSender;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;
import java.time.LocalDateTime;

public class SendTemperatureEmailScheduler implements Scheduler {

    private MailSender mailSender;
    private TemperatureReader temperatureReader;

    public SendTemperatureEmailScheduler(MailSender mailSender, TemperatureReader temperatureReader) {
        this.mailSender = mailSender;
        this.temperatureReader = temperatureReader;
    }

    @Override
    public Long getPeriod() {
        //return 3600000L;
        return 120000L;
    }

    @Override
    public void performScheduledAction() {

        //read temperature
        Double temperature = this.temperatureReader.readTemperature();

        //compose message
        String recepiet = "send.temperature.statistics@gmail.com";
        String subject = "Temperature at " + LocalDateTime.now();
        String message = "Temperature at " + LocalDateTime.now() + " is " + temperature;

        //send it
        //public void sendMail(String recepient, String subject, String body) {

        try {
            this.mailSender.sendMail(recepiet, subject, message);
        } catch (Exception e) {
            System.out.println("Exception while sending email");
        }
    }
}
