package lv.artjomsfedosenko.temperaturelogger.schedulers.custom;

public interface Scheduler {

    Long getPeriod();

    void performScheduledAction();
}
