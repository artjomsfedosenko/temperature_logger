package lv.artjomsfedosenko.temperaturelogger.schedulers.timer;

import lv.artjomsfedosenko.temperaturelogger.services.MailSender;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;

import java.time.LocalDateTime;
import java.util.TimerTask;

public class SendTemperatureEmail extends TimerTask {

    private MailSender mailSender;
    private TemperatureReader temperatureReader;

    public SendTemperatureEmail(MailSender mailSender, TemperatureReader temperatureReader) {
        super();
        this.mailSender = mailSender;
        this.temperatureReader = temperatureReader;
    }

    @Override
    public void run() {
        //read temperature
        Double temperature = this.temperatureReader.readTemperature();

        //compose message
        String recepient = "send.temperature.statistics@gmail.com";
        String subject = "Temperature at " + LocalDateTime.now();
        String message = "Temperature at " + LocalDateTime.now() + " is " + temperature;

        try {
            this.mailSender.sendMail(recepient, subject, message);
        } catch (Exception e) {
            System.out.println("Exception while sending email");
        }
    }
}
