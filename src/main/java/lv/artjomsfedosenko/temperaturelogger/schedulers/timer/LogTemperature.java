package lv.artjomsfedosenko.temperaturelogger.schedulers.timer;

import lv.artjomsfedosenko.temperaturelogger.services.FileLogWriter;
import lv.artjomsfedosenko.temperaturelogger.services.TemperatureReader;

import java.time.LocalDateTime;
import java.util.TimerTask;

public class LogTemperature extends TimerTask {

    private TemperatureReader temperatureReader;
    private FileLogWriter fileLogWriter;

    public LogTemperature(TemperatureReader temperatureReader, FileLogWriter fileLogWriter) {
        super();
        this.temperatureReader = temperatureReader;
        this.fileLogWriter = fileLogWriter;
    }

    @Override
    public void run() {
        //read temperature
        Double temperature = this.temperatureReader.readTemperature();

        //compose message
        String logMessage = LocalDateTime.now() + " temp: " + temperature;

        //write to log
        this.fileLogWriter.writeToLog(logMessage);
    }
}
